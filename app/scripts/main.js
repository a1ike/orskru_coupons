$(document).ready(function() {

  $('.oc-header__toggle').on('click', function(e) {
    e.preventDefault();
    $('.oc-sidebar').toggleClass('oc-sidebar_show');
  });

  $('.oc-sidebar__close').on('click', function(e) {
    e.preventDefault();
    $('.oc-sidebar').toggleClass('oc-sidebar_show');
  });

  $('.open-basket').on('click', function(e) {
    e.preventDefault();
    $('.oc-basket').toggle();
  });

  $('.close-basket').on('click', function(e) {
    e.preventDefault();
    $('.oc-basket').toggle();
  });

  $('.open-empty').on('click', function(e) {
    e.preventDefault();
    $('.oc-empty').toggle();
  });

  $('.close-empty').on('click', function(e) {
    e.preventDefault();
    $('.oc-empty').toggle();
  });

  $('.oc-basket__card-plus').on('click', function(e) {
    e.preventDefault();

    var currentVal = +$(this).prev().val();
    $(this).prev().val(++currentVal);
  });

  $('.oc-basket__card-minus').on('click', function(e) {
    e.preventDefault();

    var currentVal = +$(this).next().val();
    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('#basket-button-order').on('click', function(e) {
    e.preventDefault();

    $('#basket-button-close').hide();
    $('#basket-button-order').hide();
    $('#basket-button-prev').show();
    $('#basket-button-done').show();
    $('.oc-basket__cards').hide();
    $('.oc-basket__clear').hide();
    $('#basket-form').show();
  });

  $('#basket-button-prev').on('click', function(e) {
    e.preventDefault();
    
    $('#basket-button-prev').hide();
    $('#basket-button-done').hide();
    $('#basket-button-close').show();
    $('#basket-button-order').show();
    $('#basket-form').hide();
    $('.oc-basket__cards').show();
    $('.oc-basket__clear').show();
  });
});